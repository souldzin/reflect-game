# reflect-game

### How do I run this thing?

This needs parcel. If you don't have it you can run

```
yarn global add parcel
```

Then, you can simply run:

```
yarn
yarn run dev
```
