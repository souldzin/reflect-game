import { Component } from "@bytebros/ecs";

export class Life extends Component {
  constructor(life = 0) {
    super();
    this.life = life;
  }
}
