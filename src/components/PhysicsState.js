import { Component } from "@bytebros/ecs";

export class PhysicsState extends Component {
  constructor(grounded = false) {
    super();
    this.grounded = grounded;
  }
}
