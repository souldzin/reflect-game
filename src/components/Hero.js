import { Component } from "@bytebros/ecs";
import { Sprite } from "./Sprite";
import { Position } from "./Position";
import { Velocity } from "./Velocity";
import { PhysicsState } from "./PhysicsState";
import { BoxCollider } from "./BoxCollider";

export function createHero(store, heroSpritesheet) {
  store
    .createEntity()
    .addComponents(
      new Sprite(heroSpritesheet),
      new Position(50, 300),
      new Velocity(0, -200),
      new PhysicsState(),
      new BoxCollider(25, 7, 65, 115),
      new Hero()
    );
}

export class Hero extends Component {
  constructor() {
    super();
  }
}
