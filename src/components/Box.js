import { Component } from "@bytebros/ecs";
import { Velocity } from "./Velocity";
import { Position } from "./Position";

export function createBox(x, y, width, height) {
  return [new Position(x, y), new Box(width, height)];
}

export function createMovingBox(x, y, width, height) {
  return [...createBox(x, y, width, height), new Velocity()];
}

export class Box extends Component {
  constructor(width = 0, height = 0) {
    super();
    this.width = width;
    this.height = height;
  }
}
