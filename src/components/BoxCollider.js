import { Component } from "@bytebros/ecs";

export class BoxCollider extends Component {
  /**
   *
   * @param {*} offsetX where does the box collider start based on the Position
   * @param {*} offsetY
   * @param {*} width
   * @param {*} height
   */
  constructor(offsetX, offsetY, width, height) {
    super();
    this.offsetX = offsetX;
    this.offsetY = offsetY;
    this.width = width;
    this.height = height;
  }
}
