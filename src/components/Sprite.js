import { Component } from "@bytebros/ecs";
import { FPS } from "../constants";

export class Sprite extends Component {
  constructor(spritesheet) {
    super();
    this.animator = spritesheet.createAnimator();
    this.speed = 2.5 / FPS;
    this.timer = 2.5 / FPS;
  }
}
