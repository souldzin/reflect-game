import { Component } from "@bytebros/ecs";

export class RenderKey extends Component {
  constructor(key = "") {
    super();
    this.key = key;
  }
}
