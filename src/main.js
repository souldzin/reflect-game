import { ECS, EntityStore } from "@bytebros/ecs";
import { ASPECT, KEY_TOP, KEY_BOTTOM } from "./constants";
import {
  BoxShooterSystem,
  BoxLifeSystem,
  PhysicsSystem,
  RenderSystem,
  GravitySystem,
  HeroSystem
} from "./systems";
import { createLoop } from "./loop";
import {
  BoxShooter,
  createBox,
  Velocity,
  Life,
  createHero
} from "./components";
import { heroSpritesheet } from "./assets/hero";
import { loadSpritesheet } from "./spritesheet";

function getSmartCanvasDimensions({ innerWidth, innerHeight }) {
  const width = innerWidth;
  const height = width / ASPECT;

  if (height <= innerHeight) {
    return { width, height };
  }

  return {
    height: innerHeight,
    width: innerHeight * ASPECT
  };
}

function getCanvasDimensions({ innerWidth: width, innerHeight: height }) {
  return {
    width,
    height
  };
}

function createCanvases(el) {
  const top = document.createElement("canvas");
  const bottom = document.createElement("canvas");

  el.appendChild(top);
  el.appendChild(bottom);

  return { top, bottom };
}

function createDummies(store) {
  store.createEntity().addComponents(new Velocity());
  store.createEntity().addComponents(new Life());
}

function createFloor(store) {
  store.createEntity().addComponents(...createBox(0, 0, 10000, 1));
}

function createBoxShooter(store) {
  store.createEntity().addComponents(new BoxShooter());
}

function loadAssets() {
  return loadSpritesheet(heroSpritesheet).then(hero => ({ hero }));
}

function onAssetsLoaded(el, assets) {
  const canvases = createCanvases(el);
  const store = new EntityStore();

  createDummies(store);
  createFloor(store);
  createBoxShooter(store);
  createHero(store, assets.hero);

  const game = new ECS(store)
    .addSystem(new HeroSystem())
    .addSystem(new RenderSystem(canvases.top, -1, KEY_TOP))
    .addSystem(new RenderSystem(canvases.bottom, 1, KEY_BOTTOM))
    .addSystem(new BoxShooterSystem(canvases.top, store))
    .addSystem(new BoxLifeSystem())
    .addSystem(new PhysicsSystem())
    .addSystem(new GravitySystem());

  const loop = createLoop(dt => game.updateSystems(dt));

  loop.start();

  const onResize = () => {
    const { width, height } = getCanvasDimensions(window);

    canvases.top.width = width;
    canvases.bottom.width = width;
    canvases.top.height = height / 2;
    canvases.bottom.height = height / 2;
  };

  window.addEventListener("resize", onResize);

  onResize();
}

export default function() {
  const el = document.getElementById("app");

  return loadAssets().then(assets => onAssetsLoaded(el, assets));
}
