export const ASPECT = 1920 / 1080;
export const BREAK_SM = 320;
export const BREAK_MD = 700;
export const BREAK_LG = 1000;
export const FPS = 60;
export const LOOP_EPSILON = 1.5;
export const KEY_TOP = 1;
export const KEY_BOTTOM = 2;
export const GRAVITY = 600;
export const DROP_GRAVITY = -600;
export const JUMP = 400;

export const KEY_SPACE = " ";
