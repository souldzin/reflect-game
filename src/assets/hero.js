import url from "../../assets/spritesheet.png";

export const heroSpritesheet = {
  url,
  rows: 4,
  cols: 4,
  width: 125,
  height: 125
};
