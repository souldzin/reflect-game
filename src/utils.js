export function getCluster(entity, ...cluster) {
  return cluster.reduce(
    (acc, x) => Object.assign(acc, entity.getComponent(x)),
    {}
  );
}

export function shuffle(a) {
  let i, j, swp;

  for (i = a.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1));
    swp = a[i];
    a[i] = a[j];
    a[j] = swp;
  }

  return a;
}
