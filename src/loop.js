import { FPS } from "./constants";

export function createLoop(cb) {
  let reqHandle = 0;
  let nextFrameMs = 0;
  let lastTime = 0;
  let time = 0;
  let frame = 0;
  let playing = false;

  const loop = frameTime => {
    if (playing) {
      reqHandle = requestAnimationFrame(loop);

      if (frameTime < nextFrameMs) {
        return; // Skip this cycle as we are animating too quickly.
      }

      nextFrameMs = Math.max(nextFrameMs + 1000 / FPS, frameTime);
    }

    lastTime = time;
    time = frame / FPS;
    if ((time * FPS) | (0 == frame - 1)) {
      time += 0.000001;
    }
    frame++;

    cb(time - lastTime, time);
  };

  const start = () => {
    cancelAnimationFrame(reqHandle);

    playing = true;

    reqHandle = requestAnimationFrame(loop);
  };

  const stop = () => {
    playing = false;

    cancelAnimationFrame(reqHandle);
  };

  return {
    start,
    stop
  };
}
