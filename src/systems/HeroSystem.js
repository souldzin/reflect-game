import { System } from "@bytebros/ecs";
import { Hero, PhysicsState, Velocity } from "../components";
import { KEY_SPACE, JUMP } from "../constants";

export class HeroSystem extends System {
  constructor() {
    super();
    this.query = [Hero, PhysicsState, Velocity];
    this.init();
    this.jumpStarted = false;
  }

  run(dt, entities) {
    entities.forEach(entity => {
      const phys = entity.getComponent(PhysicsState);
      const vel = entity.getComponent(Velocity);

      if (this.jumpStarted && phys.grounded) {
        vel.y = JUMP;
      }
    });
  }

  init() {
    window.addEventListener("keydown", event => {
      if (event.key === KEY_SPACE) {
        this.tryJump();
      }
    });

    window.addEventListener("keyup", event => {
      if (event.key === KEY_SPACE) {
        this.tryStopJump();
      }
    });
  }

  tryJump() {
    this.jumpStarted = true;
  }

  tryStopJump() {
    this.jumpStarted = false;
  }
}
