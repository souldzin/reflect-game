import { System } from "@bytebros/ecs";
import { PhysicsState, Velocity } from "../components";
import { GRAVITY, DROP_GRAVITY } from "../constants";

export class GravitySystem extends System {
  constructor() {
    super();
    this.query = [PhysicsState, Velocity];
  }

  run(dt, entities) {
    entities.forEach(entity => {
      const phys = entity.getComponent(PhysicsState);
      const vel = entity.getComponent(Velocity);

      if (phys.grounded) {
        return;
      }

      const nextY = vel.y - dt * GRAVITY;

      vel.y = nextY;
    });
  }
}
