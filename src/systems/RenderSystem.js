import { System } from "@bytebros/ecs";
import { Box, Position, RenderKey, Sprite, BoxCollider } from "../components";

export class RenderSystem extends System {
  constructor(canvas, scale, key) {
    super();
    this.canvas = canvas;
    this.ctx = canvas.getContext("2d");
    this.scale = scale;
    this.time = 0;
    this.query = [Position];
    this.renderKey = key;
  }

  run(dt, entities) {
    this.clear();
    this.ctx.setTransform(
      1,
      0,
      0,
      this.scale,
      0,
      this.scale < 0 ? this.canvas.height : 0
    );

    entities
      .filter(x => this.includeEntity(x))
      .forEach(entity => {
        this.renderEntity(dt, entity);
      });

    this.time += dt;
  }

  renderEntity(dt, entity) {
    // DEBUG: This is helpful when debugging box collider values
    // this.renderBoxCollider(entity);

    const sprite = entity.getComponent(Sprite);

    if (sprite) {
      return this.renderSpriteEntity(dt, entity, sprite);
    }

    const box = entity.getComponent(Box);

    if (box) {
      return this.renderBoxEntity(dt, entity, box);
    }
  }

  renderBoxCollider(entity) {
    const boxCollider = entity.getComponent(BoxCollider);

    if (!boxCollider) {
      return;
    }

    const { x, y } = entity.getComponent(Position);
    const { offsetX, offsetY, width, height } = boxCollider;

    this.ctx.save();
    this.ctx.fillRect(x, y, 5, 5);
    this.ctx.strokeStyle = "#0f0";
    this.ctx.strokeRect(x + offsetX, y + offsetY, width, height);
    this.ctx.restore();
  }

  renderSpriteEntity(dt, entity, sprite) {
    const { x, y } = entity.getComponent(Position);

    sprite.timer -= dt;

    if (sprite.timer <= 0) {
      sprite.timer = sprite.speed;
      sprite.animator.next();
    }

    const {
      img,
      x: imgX,
      y: imgY,
      width: imgWidth,
      height: imgHeight
    } = sprite.animator.state();

    this.ctx.save();
    this.ctx.transform(-1, 0, 0, -1, x + imgWidth, y + imgHeight);
    this.ctx.drawImage(
      img,
      imgX,
      imgY,
      imgWidth,
      imgHeight,
      0,
      0,
      imgWidth,
      imgHeight
    );
    this.ctx.restore();
  }

  renderBoxEntity(dt, entity, { width, height }) {
    const { x, y } = entity.getComponent(Position);

    this.ctx.fillStyle = "#000";
    this.ctx.fillRect(x, y, width, height);
  }

  includeEntity(entity) {
    const { key } = entity.getComponent(RenderKey) || {};

    return !key || key === this.renderKey;
  }

  clear() {
    this.canvas.width += 0;
  }
}
