import { System } from "@bytebros/ecs";
import { Box, Life } from "../components";

export class BoxLifeSystem extends System {
  constructor() {
    super();
    this.query = [Box, Life];
  }

  run(dt, entities) {
    entities.forEach(entity => {
      const lifeComp = entity.getComponent(Life);

      lifeComp.life -= dt;

      if (lifeComp.life <= 0) {
        entity.destroy();
      }
    });
  }
}
