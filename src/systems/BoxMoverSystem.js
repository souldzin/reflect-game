import { System } from "@bytebros/ecs";
import { Box, Velocity } from "../components";

export class BoxMoverSystem extends System {
  constructor() {
    super();
    this.time = 0;
    this.query = [Box, Velocity];
  }

  run(dt, entities) {
    this.time += dt;

    entities.forEach(entity => {
      const vel = entity.getComponent(Velocity);

      vel.x = 100 * Math.sin(this.time);
    });
  }
}
