import { System } from "@bytebros/ecs";
import {
  BoxShooter,
  createBox,
  Velocity,
  RenderKey,
  Life
} from "../components";
import { KEY_TOP, KEY_BOTTOM } from "../constants";
import { shuffle } from "../utils";

const BOX_SIZE = 120;
const BOX_SPEED = -500;

export class BoxShooterSystem extends System {
  constructor(canvas, store) {
    super();
    this.canvas = canvas;
    this.store = store;
    this.query = [BoxShooter];
  }

  run(dt, entities) {
    entities.forEach(entity => {
      const shooter = entity.getComponent(BoxShooter);

      if (!shooter.running) {
        return;
      }

      shooter.timer -= dt;

      if (shooter.timer > 0) {
        return;
      }

      this.shootBoxes();
      shooter.timer = shooter.startTime + Math.random() * 3;
    });
  }

  shootBoxes() {
    const { width } = this.canvas;
    const keys = shuffle([KEY_BOTTOM, null, KEY_TOP]);

    keys.forEach((key, i) => {
      if (!key) {
        return;
      }

      this.store
        .createEntity()
        .addComponents(
          ...createBox(width, BOX_SIZE * i, BOX_SIZE, BOX_SIZE),
          new Velocity(BOX_SPEED),
          new RenderKey(key),
          new Life(10)
        );
    });
  }
}
