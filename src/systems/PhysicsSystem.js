import { System } from "@bytebros/ecs";
import { Velocity, Position, PhysicsState, BoxCollider } from "../components";

export class PhysicsSystem extends System {
  constructor() {
    super();
    this.query = [Position, Velocity];
  }

  run(dt, entities) {
    entities.forEach(entity => {
      const pos = entity.getComponent(Position);
      const vel = entity.getComponent(Velocity);

      pos.x += vel.x * dt;
      pos.y += vel.y * dt;
      pos.z += vel.z * dt;

      this.runGrounder(entity, pos);
    });
  }

  runGrounder(entity, pos) {
    const phys = entity.getComponent(PhysicsState);
    const boxCollider = entity.getComponent(BoxCollider);

    if (!phys || !boxCollider) {
      return;
    }

    const bottom = pos.y + boxCollider.offsetY;

    if (bottom <= 0) {
      phys.grounded = true;
      pos.y = -boxCollider.offsetY;
    } else {
      phys.grounded = false;
    }
  }
}
