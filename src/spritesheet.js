const createAnimator = (spritesheet, img) => {
  const state = {
    img,
    row: 0,
    col: 0,
    x: 0,
    y: 0,
    width: spritesheet.width,
    height: spritesheet.height
  };

  return {
    state() {
      return state;
    },
    next() {
      state.col += 1;

      if (state.col >= spritesheet.cols) {
        state.col = 0;
        state.row += 1;

        if (state.row >= spritesheet.rows) {
          state.row = 0;
        }
      }

      state.x = state.col * state.width;
      state.y = state.row * state.height;

      return this;
    }
  };
};

export const loadSpritesheetImage = url => {
  return new Promise(resolve => {
    const img = new Image();
    img.src = url;
    img.onload = () => {
      resolve(img);
    };
  });
};

export const loadSpritesheet = spritesheet => {
  return loadSpritesheetImage(spritesheet.url).then(img => ({
    createAnimator: () => createAnimator(spritesheet, img)
  }));
};
